<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CPageTemplate::IncludeLangFile(__FILE__);

?>
                <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"left-menu", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "31",
		"IBLOCK_TYPE" => "nacigation",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => 141,
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_LINK",
			1 => "UF_COLOR_CLASS",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "5",
		"VIEW_MODE" => "LIST",
		"COMPONENT_TEMPLATE" => "left-menu"
	),
	false
);?>