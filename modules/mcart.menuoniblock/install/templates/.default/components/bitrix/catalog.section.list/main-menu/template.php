<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<nav class="main-nav header-top__menu">
		 <div class="container">
                        <ul class="main-nav__list">
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<li
				class="main-nav__item main-nav__item_dropdown <?if(!empty($arResult["ALL_ITEMS"][$itemID]["UF_COLOR_CLASS"])) echo $arResult["ALL_ITEMS"][$itemID]["UF_COLOR_CLASS"];?>  <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>bx-active<?endif?>"
				
				<?if (is_array($arColumns) && count($arColumns) > 0):?>
					data-role="bx-menu-item"
				<?endif?>
				>
                            <a class="main-nav__link"  href="<?=$arResult["ALL_ITEMS"][$itemID]["UF_LINK"]?>">
					
						<?=$arResult["ALL_ITEMS"][$itemID]["NAME"]?>
						<?/*if (is_arra($arColumns) && count($arColumns) > 0):?>
                                <i class="fa fa-angle-down"></i><?endif*/?>
					
				</a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<div class="main-nav__submenu">
                                    <div class="main-nav__submenu-wrap">
                                       
					<?foreach($arColumns as $key=>$arRow):?>
						 <ul class="submenu">
						<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
							<li class="submenu__col">
                                                            <div class="submenu__item"> 
                                                                <a 
                                                                    class="submenu__link <?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?> bx-active<?endif;?>"
                                                                    href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["UF_LINK"]?>"
									
									data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>">
									<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["NAME"]?>
								</a>
                                                            </div>
							<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
								
								<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
							<div class="submenu__sub-item">
                                                                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["UF_LINK"]?>" class="submenu__sub-link">
                                                                        <?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["NAME"]?>
                                                                        </a>
                                                </div>		
                                                        
								<?endforeach;?>
								
							<?endif?>
							</li>
						<?endforeach;?>
						</ul>
					<?endforeach;?>
					
				</div>
				</div>
			<?endif?>
			</li>
		<?endforeach;?>
		</ul>
		
                </div>
	</nav>