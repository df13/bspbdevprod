<?

IncludeModuleLangFile(__FILE__);

Class mcart_menuoniblock extends CModule
{

    const MODULE_ID = 'mcart.menuoniblock';

    var $MODULE_ID = 'mcart.menuoniblock';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("mcart.menuoniblock_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("mcart.menuoniblock_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("mcart.menuoniblock_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("mcart.menuoniblock_PARTNER_URI");
    }

    function CheckIblockType()
    {
        CModule::IncludeModule("iblock");
        $rsDb = CIBlockType::GetList(
                        array(), array('ID' => 'navigation')
        );
        if ($arIblockType = $rsDb->GetNext())
        {
            return true;
        }
        return false;
    }

    function AddIblockType()
    {
        CModule::IncludeModule("iblock");
        global $DB;
        $arFields = array(
            'ID' => 'navigation_mcart',
            'SECTIONS' => 'Y',
            'SECTION_PROPERTY' => 'Y',
            'IN_RSS' => 'N',
            'SORT' => 100,
            'LANG' => array(
                'ru' => array(
                    'NAME' => GetMessage("mcart.menuoniblock_IBLOCK_TYPE_NAME"),
                    'SECTION_NAME' => GetMessage("mcart.menuoniblock_IBLOCK_SECTION_NAME"),
                    'ELEMENT_NAME' => ''
                )
            )
        );

        $obBlocktype = new CIBlockType;
        $DB->StartTransaction();
        $res = $obBlocktype->Add($arFields);
        if (!$res)
        {
            $DB->Rollback();
            echo 'Error: ' . $obBlocktype->LAST_ERROR . '<br>';
        } else
        {
            $DB->Commit();
        }
    }

    function CheckIblock()
    {
        CModule::IncludeModule("iblock");
        $rsDb = CIBlock::GetList(
                        array(), array('CODE' => 'navigation')
        );
        if ($arIblock = $rsDb->GetNext())
        {
            return $arIblock["ID"];
        }
        return false;
    }

    function GetSiteList()
    {
        $arSites = array();
        $rsDb = CSite::GetList();
        while ($arSite = $rsDb->Fetch())
        {
            $arSites[] = $arSite["LID"];
        }

        if (!empty($arSites))
        {
            return $arSites;
        } else
        {
            return false;
        }
    }

    function AddIblock($pArSites)
    {
        CModule::IncludeModule("iblock");
        $iblockObj = new CIBlock;
        $arFields = Array(
            "ACTIVE" => "Y",
            "NAME" => GetMessage("mcart.menuoniblock_IBLOCK_NAME"),
            "CODE" => "portal_menu",
            "LIST_PAGE_URL" => "",
            "DETAIL_PAGE_URL" => "",
            "IBLOCK_TYPE_ID" => "navigation_mcart",
            "SITE_ID" => $pArSites,
            "SORT" => 100,
            "GROUP_ID" => Array("2" => "D", "3" => "R")
        );

        $iblockId = $iblockObj->Add($arFields);
        return $iblockId;
    }
    
    function GetSectionProps($pIblockId)
    {
      CModule::IncludeModule("iblock");
      $arSectionProp = array();
      $rsDb = CUserTypeEntity::GetList(array(), array("ENTITY_ID" =>"IBLOCK_" . $pIblockId . "_SECTION"));
      while($arProp = $rsDb->GetNext())
      {
          $arSectionProp[] = $arProp["FIELD_NAME"];
      }
       if (!empty($arSectionProp))
        {
            return $arSectionProp;
        } else
        {
            return false;
        }
    }

    function AddSectionProps($pArProps, $pIblockId)
    {
        CModule::IncludeModule("iblock");
        $obUserType = new CUserTypeEntity();
if(!in_array("UF_LINK", $pArProps))
{
        $fieldTitle = GetMessage("mcart.menuoniblock_IBLOCK_SECTION_PROP_LINK");
        $arPropFields = array(
            "ENTITY_ID" => "IBLOCK_" . $pIblockId . "_SECTION",
            "FIELD_NAME" => "UF_LINK",
            "USER_TYPE_ID" => "string",
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => "N", 
            "MANDATORY" => "Y", // required 
            "SHOW_FILTER" => "S",
            "SHOW_IN_LIST" => "Y",
            "EDIT_IN_LIST" => "Y",
            "IS_SEARCHABLE" => "N",
            "SETTINGS" => array(
                "SIZE" => "70", // field lenght
                "ROWS" => "1" //  field height
            ),
            "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
        );
        $FIELD_ID = $obUserType->Add($arPropFields);
}

if(!in_array("UF_COLOR_CLASS", $pArProps))
{
        $fieldTitle = GetMessage("mcart.menuoniblock_IBLOCK_SECTION_PROP_COLOR_CLASS");
        $arPropFields = array(
            "ENTITY_ID" => "IBLOCK_" . $pIblockId . "_SECTION",
            "FIELD_NAME" => "UF_COLOR_CLASS",
            "USER_TYPE_ID" => "string",
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => "N", 
            "MANDATORY" => "N", // required 
            "SHOW_FILTER" => "S",
            "SHOW_IN_LIST" => "Y",
            "EDIT_IN_LIST" => "Y",
            "IS_SEARCHABLE" => "N",
            "SETTINGS" => array(
                "SIZE" => "70", // field lenght
                "ROWS" => "1" //  field height
            ),
            "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
        );
        $FIELD_ID = $obUserType->Add($arPropFields);
}

if(!in_array("UF_LEFT_MENU", $pArProps))
{
        $fieldTitle = GetMessage("mcart.menuoniblock_IBLOCK_SECTION_PROP_UF_LEFT_MENU");
        $arPropFields = array(
            "ENTITY_ID" => "IBLOCK_" . $pIblockId . "_SECTION",
            "FIELD_NAME" => "UF_LEFT_MENU",
            "USER_TYPE_ID" => "boolean",
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => "N", 
            "MANDATORY" => "N", // required 
            "SHOW_FILTER" => "S",
            "SHOW_IN_LIST" => "Y",
            "EDIT_IN_LIST" => "Y",
            "IS_SEARCHABLE" => "N",
            "SETTINGS" => array(
                "DEFAULT_VALUE" => 0
            ),
            "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
            "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
        );
        $FIELD_ID = $obUserType->Add($arPropFields);
}

    }

    

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles($arParams = array())
    {
       CopyDirFiles(
               __DIR__."/templates",
               $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates",
               true,
               true
               );
       return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/catalog.section.list/main-menu");
        DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/catalog.section.list/left-menu");
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
       
        $iblockId = null;
        $arProps = array();
        $arSites = $this->GetSiteList();
        
        $this->InstallFiles();
       
        
        if(!$this->CheckIblockType())
        {
            $this->AddIblockType();
        }
        if(!$this->CheckIblock())
        {
           $iblockId =  $this->AddIblock($arSites);
        }
        
        $arProps = $this->GetSectionProps($iblockId);
        
        $this->AddSectionProps($arProps, $iblockId);
    
        RegisterModule(self::MODULE_ID);
    }

    function DoUninstall()
    {
        global $APPLICATION;
        UnRegisterModule(self::MODULE_ID);
       
        $this->UnInstallFiles();
    }

}

?>
