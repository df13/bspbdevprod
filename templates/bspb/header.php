<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if ($_GET['RELOAD'] != 'Y' && $_GET['IFRAME'] != 'Y'):
CUtil::InitJSCore();
    ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <? IncludeTemplateLangFile(__FILE__); ?>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
        <head>
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta charset="UTF-8" />
<?
            $APPLICATION->ShowHead();
            CJSCore::Init(array("jquery"));
            $asset = \Bitrix\Main\Page\Asset::getInstance();
            ?>
          <!--[if (gt IE 9)|!(IE)]><!-->
        <?
        $asset->addJs(SITE_TEMPLATE_PATH . '/static/js/packery-docs.min.js');
            $asset->addString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/static/css/main.min.css"/>', false, \Bitrix\Main\Page\AssetLocation::AFTER_CSS);
        ?>
        <!--<![endif]-->
         
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <meta property="og:image" content="" />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="500" />
        <meta property="og:image:height" content="300" />
        <meta property="twitter:description" content="" />
        <link rel="image_src" href="" />
        <!--<link rel="icon" type="image/x-icon" href="favicon.ico" />-->
            
      
        <script>
            (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
        </script>
        
         <title><? $APPLICATION->ShowTitle() ?></title>
        </head>
<? endif ?>
    <body class="page">
         <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
        <div class="page__wrapper">
            <header class="header-top">
        <div class="container header-top__inner">
                    <a href="/" class="logo header-top__logo">
                        <img src="<?= SITE_TEMPLATE_PATH;?>/static/img/content/logo.png" alt="<?= GetMessage("PORTAL_NAME");?>" class="logo__img" title="" />
                    </a>
            <?
                $APPLICATION->IncludeComponent("bitrix:search.title", "search", Array(
                    "NUM_CATEGORIES" => "5",
                    "TOP_COUNT" => "5",
                    "CHECK_DATES" => "N",
                    "SHOW_OTHERS" => "Y",
                    "PAGE" => "/search/index.php",
                    "CATEGORY_0_TITLE" => GetMessage("LIGHT_HEADER_SEARCH_EMPLOYEE"),
                    "CATEGORY_0" => array(
                        0 => "intranet",
                    ),
                    "CATEGORY_1_TITLE" => GetMessage("LIGHT_HEADER_SEARCH_DOCS"),
                    "CATEGORY_1" => array(
                        0 => "iblock_library",
                    ),
                    "CATEGORY_1_iblock_library" => array(
                        0 => "all",
                    ),
                    "CATEGORY_2_TITLE" => GetMessage("LIGHT_HEADER_SEARCH_GROUPS"),
                    "CATEGORY_2" => array(
                        0 => "socialnetwork",
                    ),
                    "CATEGORY_2_socialnetwork" => array(
                        0 => "all",
                    ),
                    "CATEGORY_3_TITLE" => GetMessage("LIGHT_HEADER_SEARCH_MICROBLOG"),
                    "CATEGORY_3" => array(
                        0 => "microblog",
                    ),
                    "CATEGORY_4_TITLE" => "CRM",
                    "CATEGORY_4" => array(
                        0 => "crm",
                    ),
                    "CATEGORY_OTHERS_TITLE" => GetMessage("LIGHT_HEADER_SEARCH_OTHER"),
                    "SHOW_INPUT" => "Y",
                    "INPUT_ID" => "title-search-input",
                    "CONTAINER_ID" => "title-search",
                        ), false
                );
                ?>
            
            <!--user profile-->
            
                    <div class="header-top__notification">
                        <?
                $APPLICATION->IncludeComponent(
                        "bitrix:main.user.link", "user_info", array(
                    "CACHE_TIME" => "7200",
                    "CACHE_TYPE" => "A",
                    "COMPONENT_TEMPLATE" => "sidebar",
                    "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
                    "ID" => $USER->GetID(),
                    "NAME_TEMPLATE" => "#NAME_SHORT# #SECOND_NAME_SHORT# #LAST_NAME#",
                    "PATH_TO_SONET_USER_PROFILE" => "",
                    "PROFILE_URL" => "/company/personal/user/#ID#/",
                    "SHOW_FIELDS" => array(
                    ),
                    "SHOW_LOGIN" => "Y",
                    "SHOW_YEAR" => "Y",
                    "THUMBNAIL_LIST_SIZE" => "100",
                    "USER_PROPERTY" => array(
                    ),
                    "USE_THUMBNAIL_LIST" => "Y"
                        ), false
                );
                ?>
                         <!--icons telephones and notifications-->
                        <div class="header-top__notification-phone">
                        </div>
                        <div class="header-top__notification-message">
                            <span class="header-top__notification-count">29</span>
                        </div>
                         <!--end-->
                    </div>
                </div>
                <!--main menu-->
                <?$curSectId = $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "main-menu", Array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"IBLOCK_ID" => "31",	// Инфоблок
		"IBLOCK_TYPE" => "nacigation",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "",
			1 => "",
		),
		"SECTION_ID" => 141,	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_LINK",
			1 => "UF_COLOR_CLASS",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
		"TOP_DEPTH" => "5",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LIST",	// Вид списка подразделов
                
	),
	false
);?>
                 <?
               
            $APPLICATION->IncludeComponent("bitrix:menu", "catalog_horizontal", array(
	"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "86400",
		"MAX_LEVEL" => "3",
		"COMPONENT_TEMPLATE" => "catalog_horizontal",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_THEME" => "wood"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);
            ?>
                
              
            </header>
            <div class="container">
                
               <?if($APPLICATION->GetCurPage(true) != "/index.php"):?>
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false
);?>
                

               
               <? endif; ?>
 
                
                
                
                
                
                
                
                
                
                
                
                
                
                
         