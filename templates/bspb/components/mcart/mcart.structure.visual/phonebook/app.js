var app = {
	checkbox: function( element )
	{
		if( $(element).find('input').attr('checked') === true || $(element).find('input').attr('checked') == 'checked' )
		{
			$(element).find('.checkbox').addClass('checkbox-checked');
			$(element).addClass('checkbox-label-checked');
		}
		else
		{
			$(element).find('.checkbox').removeClass('checkbox-checked');
			$(element).removeClass('checkbox-label-checked');
		}
                checkboxChange();
	},
	onCheckbox: function( id )
	{
		if( $('#' + id).find('.checkbox-label').length > 0 )
		{
			$('#' + id).find('.checkbox-label').each(function(){
				if( $(this).find('input').attr('checked') === true || $(this).find('input').attr('checked') == 'checked' )
				{
					$(this).find('.checkbox').addClass('checkbox-checked');
					$(this).addClass('checkbox-label-checked');
				}
				else
				{
					$(this).find('.checkbox').removeClass('checkbox-checked');
					$(this).removeClass('checkbox-label-checked');
				}
			});
		}
	},
        
};

function checkboxChange( )
{
    $(".searched-item").fadeOut('fast');
    var selected = 0;
    $(".checkbox-checked").find("input").each(function(){
        $("."+$(this).val() ).fadeIn('fast');
        selected++;
    });
    if(selected==0)
    {
        $(".searched-item").fadeIn();
    }
}
$(document).ready(function(){
    (function($) {
    $.app = {
        run: function()
        {
        	app.onCheckbox('sort-1');
                //app.onCheckbox('sort-2');

            $('body').on('click', '.tree-item', function(e){
            	//e.preventDefault();
            	if( $(this).closest('.tree-item-container').find('.tree-item-container-box').length > 0 )
                {
                	var $block = $(this).closest('.tree-item-container');
                	$block.toggleClass('tree-item-container-active');
                }
                $(this).parents(".tree-item-container").addClass("tree-item-container-active");
                //return false;
            });
        }
    };

    $.app.run();
})(jQuery);
})
