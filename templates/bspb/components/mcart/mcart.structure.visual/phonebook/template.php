<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if(!empty($arResult["ENTRIES"])):?>

<div class="conteiner">
    <div class="contentbox">
        <div class="content content-library">
            
            
            <?$counter=0;$previousLevel = 0;?>
            <?foreach($arResult["ENTRIES"] as $arItem):?>
                
                <?if($arItem["DEPTH_LEVEL"]==1):?>
                    <div class="tree-item-container tree-item-container-active">
                        <a  href="<?= $APPLICATION->GetCurPage()?>" class="tree-item has-child"><?=$arItem["NAME"]?></a>
                    <div class="tree-item-container-box">
                <?endif?>
            
                <?if($arItem["DEPTH_LEVEL"]>1):?>
                        
                    <?if($counter!="1"):?>
                        
                        <?if ($arItem["DEPTH_LEVEL"] < $previousLevel || $previewParent==$arItem["IBLOCK_SECTION_ID"]):?>
                            <?if($arItem["DEPTH_LEVEL"] < $previousLevel):?>
                                <?=str_repeat("</div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]+1));?>
                            <?elseif($previewParent==$arItem["IBLOCK_SECTION_ID"]):?>
                                </div></div>
                            <?endif?>
                        <?endif?>
            
                    <?endif?>  
            
                    <?if($previousLevel!=$arItem["DEPTH_LEVEL"] || $previewParent==$arItem["IBLOCK_SECTION_ID"]):?>
                        <div class="tree-item-container <?if($arItem["DEPTH_LEVEL"]<2):?>tree-item-container-active<?endif?>">
                            <a data-depart_id ="<?= $arItem["ID"];?>" href="<?= $APPLICATION->GetCurPage()?>?current_filter=adv&users_UF_DEPARTMENT=<?= $arItem["ID"]?>" class="tree-item <?if($arItem["HAS_CHILD"] == "Y") echo 'has-hild';?>"><?=$arItem["NAME"]?></a>
                        <div class="tree-item-container-box">
                    <?endif?>
                        
                            
                            
                        <?/*if( !empty($arItem["EMPLOYEES"])):?>
                            <?foreach($arItem["EMPLOYEES"] as $i=>$arEmployee):?>

                                <?$anchor_id=randString(8)?>
                                <script type="text/javascript">
                                        BX.tooltip(<?=$arEmployee["ID"]?>, "anchor_<?=$anchor_id?>", "<?=$APPLICATION->GetCurPage(false)?>");
                                </script>
                                <div class="companions-user clearfix">
                                        <div class="companions-user-asc">
                                                <a href="<?=$arEmployee["PROFILE_URL"]?>" id="anchor_<?=$anchor_id?>" class="companions-user-name"><?=$arEmployee["NAME"]?></a>
                                        </div>
                                        <div class="companions-user-desc">
                                                <a href="mailto:<?=$arEmployee["EMAIL"]?>"><?=$arEmployee["EMAIL"]?></a>, <?=$arEmployee["WORK_POSITION"]?>, <?=$arEmployee["WORK_PHONE"]?> 
                                                <?if($arEmployee["ONLINE"]):?><span class="companions-user-status"><?=GetMessage("ONLINE")?></span><?else:?><span class="companions-user-status companions-user-offline"><?=GetMessage("OFFLINE")?></span><?endif?>
                                        </div>
                                </div>
                            <?endforeach?>
                        <?endif*/?>
                           
  
                <?endif?>
            
            
                <?if($counter==count($arResult["ENTRIES"])-1):?>
                    </div>
                    </div>
                <?endif?>

                <?if($arItem["DEPTH_LEVEL"]==1){$previewParent=$arItem["ID"];}else{$previewParent=$arItem["IBLOCK_SECTION_ID"];}?>
                <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
                <?$counter++;?>
            
            <?endforeach?>
            
        </div>
    </div>
</div>
<?endif;?>