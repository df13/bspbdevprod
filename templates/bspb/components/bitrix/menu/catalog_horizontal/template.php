<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();
?>
<nav class="main-nav header-top__menu">
		 <div class="container">
                        <ul class="main-nav__list">
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
			<li
				class="main-nav__item main-nav__item_dropdown  <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>bx-active<?endif?>"
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
				<?if (is_array($arColumns) && count($arColumns) > 0):?>
					data-role="bx-menu-item"
				<?endif?>
				onclick="if (BX.hasClass(document.documentElement, 'bx-touch')) obj_<?=$menuBlockId?>.clickInMobile(this, event);"
			>
                            <a class="main-nav__link"
					href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
					<?if (is_array($arColumns) && count($arColumns) > 0 && $existPictureDescColomn):?>
						onmouseover="obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemID?>');"
					<?endif?>
				>
					
						<?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?>
						<?/*if (is_array($arColumns) && count($arColumns) > 0):?>
                                <i class="fa fa-angle-down"></i><?endif*/?>
					
				</a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<div class="main-nav__submenu">
                                    <div class="main-nav__submenu-wrap">
                                       
					<?foreach($arColumns as $key=>$arRow):?>
						 <ul class="submenu">
						<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
							<li class="submenu__col">
                                                            <div class="submenu__item"> 
                                                                <a 
                                                                    class="submenu__link <?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?> bx-active<?endif;?>"
                                                                    href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
									<?if ($existPictureDescColomn):?>
										onmouseover="obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_2?>');"
									<?endif?>
									data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>">
									<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>
								</a>
                                                            </div>
							<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
								
								<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
							<div class="submenu__sub-item">
                                                                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>" class="submenu__sub-link">
                                                                        <?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?>
                                                                        </a>
                                                </div>		
                                                        
								<?endforeach;?>
								
							<?endif?>
							</li>
						<?endforeach;?>
						</ul>
					<?endforeach;?>
					
				</div>
			<?endif?>
			</li>
		<?endforeach;?>
		</ul>
		
                </div>
	</nav>


<script>
	BX.ready(function () {
		window.obj_<?=$menuBlockId?> = new BX.Main.Menu.CatalogHorizontal('<?=CUtil::JSEscape($menuBlockId)?>', <?=CUtil::PhpToJSObject($arResult["ITEMS_IMG_DESC"])?>);
	});
</script>