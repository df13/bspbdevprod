<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if(!empty($arResult["User"]["PERSONAL_PHOTO"]) && $arParams["THUMBNAIL_LIST_SIZE"]>0)
{
    $arResult["User"]["PERSONAL_PHOTO"] = CFile::ResizeImageGet(
            $arResult["User"]["PERSONAL_PHOTO"],
            array("width"=>$arParams["THUMBNAIL_LIST_SIZE"],"height"=>$arParams["THUMBNAIL_LIST_SIZE"]),
            BX_RESIZE_IMAGE_PROPORTIONAL
            );
}