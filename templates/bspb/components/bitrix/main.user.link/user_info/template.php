<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (strlen($arResult["FatalError"]) > 0): ?>

    <span class='errortext'><?= $arResult["FatalError"] ?></span><br /><br />
<? else: ?>

   
    <a href="<?= SITE_DIR . "company/personal/user/" . $arResult["User"]["ID"] . "/" ?>" class="header-top__user">
            <span class="header-top__user-avatar">
                <?
                $photoSRC = $arResult["User"]["PERSONAL_PHOTO"]["src"];
                if (empty($photoSRC))
                {
                    $photoSRC = $templateFolder . "/images/nopic49.jpg";
                }
                ?>
                <img src="<?= $photoSRC ?>" alt="<?= $arResult["User"]["NAME_FORMATTED"] ?>" class="main-header_user-avatar-img">
            </span>
            <span class="header-top__user-name"><?= $arResult["User"]["NAME_FORMATTED"] ?></span>
        </a>

<? endif; ?>