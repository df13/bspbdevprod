<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<aside class="menu-left">
    <ul class="menu-left__list">
<?
$counter = 0;
foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>     <!-- first level-->
            <li
                class="menu-left__item menu-left__item_dropdown <?if( $arResult["ALL_ITEMS"][$itemID]["CURRENT"]  == "Y") echo 'menu-left__item_active';?>   <? if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]): ?>bx-active<? endif ?>"
               >
                <a class="menu-left__link <? if (!empty($arResult["ALL_ITEMS"][$itemID]["UF_COLOR_CLASS"])) echo $arResult["ALL_ITEMS"][$itemID]["UF_COLOR_CLASS"]; ?>"  href="<?= $arResult["ALL_ITEMS"][$itemID]["UF_LINK"] ?>">

                    <?= $arResult["ALL_ITEMS"][$itemID]["NAME"]; $counter ++; ?>
                </a>
    <? if (is_array($arColumns) && count($arColumns) > 0): ?>
                        <? foreach ($arColumns as $key => $arRow): ?>
                        <ul class="menu-left__submenu-2lvl">
            <? foreach ($arRow as $itemIdLevel_2 => $arLevel_3): ?>  <!-- second level-->
            <li class="menu-left__submenu-2lvl-item  <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?> menu-left__submenu-2lvl-item_dropdown<?  endif;?>">

                                    <a 
                                        class="menu-left__submenu-2lvl-link <? if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]): ?> bx-active<? endif; ?>"
                                        href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["UF_LINK"] ?>">
                <?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["NAME"] ?>
                                    </a>

                                        <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                                        <ul class="menu-left__submenu-3lvl">
                    <? foreach ($arLevel_3 as $itemIdLevel_3): ?>	<!-- third level-->
                                                <li class="menu-left__submenu-3lvl-item">
                                                    <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["UF_LINK"] ?>" class="menu-left__submenu-3lvl-link">
                        <?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["NAME"] ?>
                                                    </a>
                                                </li>
                                        <? endforeach; ?>
                                          </ul>
                                <? endif ?>
                                              
                                </li>
                        <? endforeach; ?>
                        
                    <? endforeach; ?>
</ul>
            <? endif ?>
            </li>
<? endforeach; ?>
    </ul>
</aside>