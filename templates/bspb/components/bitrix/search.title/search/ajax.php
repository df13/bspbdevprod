<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$result = \Bitrix\Main\UserTable::getList(array( 
'select' => array('ID'), 
'filter' => array('UF_DEPARTMENT'=> false) 
)); 

while ($row = $result->fetch()) 
{ 
$arrNoDepartmentUsers[] = $row['ID'];
}



if(!empty($arResult["CATEGORIES"])):?>
    <div class="title-search-result" id="searchfield-tooltip">
        <div class="searchfield-tooltip">
        <ul class="searchfield-tooltip__list">
            <?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>

                    <?foreach($arCategory["ITEMS"] as $i => $arItem):?>

<?
if($arItem['PARAM1']=='USER')
		{

			//var_dump($arItem);

			if (empty($arItem['PARAM2'])) 
			{
$userId = 0;
if (substr($arItem['ITEM_ID'],0,1)=="U")
	$userId = substr($arItem['ITEM_ID'],1);

}
			 else $userId = $arItem['PARAM2'];
			if (in_array($userId, $arrNoDepartmentUsers))
				continue;
		}
?>

                        <?$arItem["NAME"] = str_replace("<b>",'<span class="blue">',$arItem["NAME"]);
                        $arItem["NAME"] = str_replace("</b>",'</span>',$arItem["NAME"]);
                        ?>
                    <li class="searchfield-tooltip__list-item">
                        <a href="<?echo $arItem["URL"]?>" class="searchfield-tooltip__link"><?=$arItem["NAME"]?></a>
                    </li>

                    <?endforeach;?>
            <?endforeach;?>
        </ul>
        </div>
    </div>
<script>
    var content = document.getElementById("searchfield-tooltip").innerHTML;
    $("#title-search").find(".searchfield-tooltip").remove();
    $("#title-search").append(content);
    $("#title-search").find(".searchfield-tooltip").css("display","block");
</script>
<?endif;
?>