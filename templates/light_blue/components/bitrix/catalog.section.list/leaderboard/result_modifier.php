<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["SECTIONS"]))
{
    $arFilter = array();
    // get iblock elements
    	$arFilter = array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'], 
            'ACTIVE' => 'Y', 
            '!SECTION_ID' => false, 
            '<=DATE_ACTIVE_FROM' => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL"))), 
            '>=DATE_ACTIVE_TO' => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")))
             );
             

	$dbRes = CIBlockElement::GetList(
		array('active_from' => 'desc', 'active_to' => 'asc'),
		$arFilter,
		false,
		false,
		array(
                    'ID','IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 
                    'PREVIEW_TEXT', 'PREVIEW_TEXT_TYPE', 'DETAIL_TEXT', 
                    'PROPERTY_USER', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO')
	);

	$arResult['ELEMENTS'] = array();
	$arUserIDs = array();
	while ($arRes = $dbRes->GetNext())
	{
		$arUserIDs[] = $arRes['PROPERTY_USER_VALUE'];
		$arResult['ELEMENTS'][$arRes["IBLOCK_SECTION_ID"]][] = $arRes;
	}
	unset($dbRes);
       
    // get user info 
    $dbUsers = CUser::GetList(
			$by = 'ID',
			$order = 'asc',
			array('ID' => implode('|', $arUserIDs), 'ACTIVE' => 'Y', '!UF_DEPARTMENT' => false),
			array('SELECT' => array('UF_*'))
		);
		while ($arUser = $dbUsers->Fetch())
		{
                
                $arUser['DETAIL_URL'] = str_replace(array('#ID#', '#USER_ID#'), $arUser['ID'], $arParams['DETAIL_URL']);

			if (!$arUser['PERSONAL_PHOTO'])
			{
				switch ($arUser['PERSONAL_GENDER'])
				{
					case "M":
						$suffix = "male";
						break;
					case "F":
						$suffix = "female";
						break;
					default:
						$suffix = "unknown";
				}
				$arUser['PERSONAL_PHOTO'] = COption::GetOptionInt("socialnetwork", "default_user_picture_".$suffix, false, SITE_ID);
			}

			$arResult['USERS'][$arUser['ID']] = $arUser;
		}
		unset($dbUsers);
                
                // make array structure
                
                foreach ($arResult["SECTIONS"] as &$arSection)
                {
                    foreach ($arResult["ELEMENTS"][$arSection["ID"]] as $arElement)
                    {
                        if($arElement["IBLOCK_SECTION_ID"] == $arSection["ID"] && array_key_exists($arElement["PROPERTY_USER_VALUE"], ($arResult['USERS'])))
                        {
                            $arSection["USERS"][$arElement["PROPERTY_USER_VALUE"]] = $arResult['USERS'][$arElement["PROPERTY_USER_VALUE"]];
                        }
                    } 
                }
                //PR($arResult["ELEMENTS"]);
    
}
?>